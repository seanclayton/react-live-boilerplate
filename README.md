># Babel 6 Not Supported Yet

# React Live Boilerplate

A Webpack boilerplate with:

* hot reloading React components;
* error handling inside component `render()` function;

Built with **[babel-plugin-react-transform](https://github.com/gaearon/babel-plugin-react-transform)** and a few custom transforms.  
**[Does not]** use React Hot Loader.

[Does not]: https://medium.com/@dan_abramov/the-death-of-react-hot-loader-765fa791d7c4

```shell
npm install
npm start
open http://localhost:3000
```

Then go ahead and edit files inside `src` (any file except `index.js`).

## Intended Use

This project started as a way for me to easily start up small React testbed within a server, with live hot-reloading and ES2015+ support.
I usually use the project like so:

1. Clone the repo
1. Create a new branch made for the thing I want to play with
1. Relish in the ease of development :smile_cat:

## What’s Inside

The component instrumentation is implemented on top of **[babel-plugin-react-transform](https://github.com/gaearon/babel-plugin-react-transform)**:

* **[react-transform-hmr](https://github.com/gaearon/react-transform-hmr)** handles hot reloading
* **[react-transform-catch-errors](https://github.com/gaearon/react-transform-catch-errors)** catches component errors

The syntax errors are displayed in an overlay by **[@glenjamin](https://github.com/glenjamin)**’s **[webpack-hot-middleware](https://github.com/glenjamin/webpack-hot-middleware)** which replaces Webpack Dev Server.

## Troubleshooting

### I can’t serve images / use different HTML file / etc

This boilerplate is just a Webpack bundle served by an Express server. It’s not meant to demonstrate every feature of either project. Please consult Webpack and Express docs to learn how to serve images, or bundle them into your JavaScript application.

## License

ISC
